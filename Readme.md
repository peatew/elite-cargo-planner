﻿** Elite Cargo Planner ** 
https://bitbucket.org/peatew/elite-cargo-planner/

Helps you plan and optimise you cargo routes. 
Elite cargo planner is a companion application that I run on a separate computer when I am playing Elite: Dangerous. 
It is simply a stand alone spread program that lets you compare commodity prices in places you have visited. 
** This is not a cheat and only comes with a small amount of example data (around Ross 210). ** 

** Attention This program requires an amount of data entry! **

Todo: 

* Places tagged with galactic coords 
* Price history
* Min / Max (SD) prices over various time periods (hour, day, week, month, year)
* Webservice / OCR
* Search
* Graphing


Elite Cargo Planner was created using assets and imagery from Elite: Dangerous, with the permission of Frontier Developments plc, for non-commercial purposes. 
It is not endorsed by nor reflects the views or opinions of Frontier Developments and no employee of Frontier Developments was involved in the making of it.
http://www.elitedangerous.com/