﻿using System;
using System.Collections.Generic;
using System.IO;

namespace EliteCargoPlanner
{
	static class StaticObject
	{
		public static readonly Dictionary<string, Commodity> Commodities = new Dictionary<string, Commodity>();

		public static readonly Dictionary<string, Market> Markets = new Dictionary<string, Market>();

		public static Options Options { get; private set; }

		public static string Notes = String.Empty; 

		public static void Load()
		{
			Options = new EliteCargoPlanner.Options();

			if (File.Exists(Helper.ResolvePath("~/Notes.txt")) == true)
			{
				Notes = Helper.ReadAllText("~/Notes.txt"); 
			}

			Helper.EnsurePathExists(Helper.ResolvePath("~/MarketData/")); 

			Commodities.Clear(); 
			Markets.Clear();

			DirectoryInfo folderInfo = new DirectoryInfo(Helper.ResolvePath("~/MarketData/"));

			if (File.Exists(Helper.ResolvePath("~/MarketData/commodities.csv")) == true)
			{
				string[] lines = Helper.ReadAllLines("~/MarketData/Commodities.csv");

				foreach (string line in lines)
				{
					if (Helper.IsNullOrEmpty(line) == true)
					{
						continue; 
					}

					string[] args = Helper.SplitCsv(line); 

					if (args.Length < 2) 
					{
						continue; 
					}

					string name = args[0];
					string valueStr = args[1];

					if (Commodities.ContainsKey(name) == false)
					{
						Commodities.Add(name, new Commodity(name)); 
					}

					float value; 

					if (float.TryParse(valueStr, out value) == false) 
					{
						continue; 
					}

					Commodities[name].Average = value; 
				}
			}

			foreach (FileInfo file in folderInfo.GetFiles())
			{
				if (file.Name.ToLower() == "commodities.csv")
				{
					continue;
				}

				if (file.Name.ToLower().EndsWith(".csv") == false)
				{
					continue; 
				}

				string marketName = file.Name.Substring(0, file.Name.Length - 4);

				if (Markets.ContainsKey(marketName) == false)
				{
					Markets.Add(marketName, new Market(marketName));
				}

				Market market = Markets[marketName];

				string[] lines = Helper.ReadAllLines(file.FullName);

				foreach (string line in lines)
				{
					if (Helper.IsNullOrEmpty(line) == true)
					{
						continue;
					}

					string[] args = Helper.SplitCsv(line);

					if (args.Length < 3)
					{
						continue;
					}

					string name = args[0];
					string sellStr = args[1];
					string buyStr = args[2];					

					if (market.Commodities.ContainsKey(name) == false)
					{
						market.Commodities.Add(name, new MarketCommodity(name));
					}

					float sell, buy;

					if (float.TryParse(sellStr, out sell) == false)
					{
						continue;
					}

					if (float.TryParse(buyStr, out buy) == false)
					{
						continue;
					}
					
					market.Commodities[name].Sell = sell;
					market.Commodities[name].Buy = buy;
				}
			}

			foreach (FileInfo file in folderInfo.GetFiles())
			{
				if (file.Name.ToLower().EndsWith(".txt") == false)
				{
					continue;
				}

				string marketName = file.Name.Substring(0, file.Name.Length - 4);

				if (Markets.ContainsKey(marketName) == false)
				{
					continue; 
				}

				Market market = Markets[marketName];

				market.Notes = Helper.ReadAllText(file.FullName);
			}
		}	

		public static void SaveAll()
		{
			Options.Save();

			if (Helper.IsNotNullOrEmpty(Notes) == true)
			{
				Helper.WriteAllText("~/Notes.txt", Notes);
			}

			List<string> commoditiesLines = new List<string>();

			foreach (Commodity comm in Commodities.Values)
			{
				commoditiesLines.Add(Helper.ToCsv(comm.Name, comm.Average)); 
			}

			Helper.WriteAllLines("~/MarketData/Commodities.csv", commoditiesLines.ToArray());

			foreach (Market market in Markets.Values)
			{
				List<string> marketLines = new List<string>();

				foreach (MarketCommodity comm in market.Commodities.Values)
				{
					marketLines.Add(Helper.ToCsv(comm.Name, comm.Sell, comm.Buy));
				}

				string marketFileName = market.Name;

				foreach (char c in Path.GetInvalidFileNameChars())
				{
					marketFileName = marketFileName.Replace(c, '-');
				}

				Helper.WriteAllLines("~/MarketData/" + marketFileName + ".csv", marketLines.ToArray());

				if (Helper.IsNotNullOrEmpty(market.Notes) == true)
				{
					Helper.WriteAllText("~/MarketData/" + marketFileName + ".txt", market.Notes);
				}
			}
		}

		public static event EventHandler MarketsChanged; 

		public static void UpdateMarkets()
		{
			if (MarketsChanged != null)
			{
				MarketsChanged(null, EventArgs.Empty); 
			}
		}
	}
}
