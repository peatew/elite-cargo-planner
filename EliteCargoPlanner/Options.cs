﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.IO;
using System.Xml;

namespace EliteCargoPlanner
{
	class Options
	{
		/// <summary>
		/// The location of the options file 
		/// </summary>
		private static string m_OptionsFileName = "~/Options.xml";
		
		/// <summary>
		/// The current state of the window (maximised or normal) 
		/// </summary>
		public FormWindowState WindowState { get; set; }

		/// <summary>
		/// The bounds of the window on the desktop
		/// </summary>
		public Rectangle Bounds { get; set; }

		public string From { get; set; }

		public string To { get; set; }

		public int Credits { get; set; }
		
		public int TotalHoldSpace { get; set; }
		
		public int UsedHoldSpace { get; set; }

		public string SortColumn { get; set; }

		public SortOrder SortDirection { get; set; }

		/// <summary>
		/// Load the options 
		/// </summary>
		public Options()
		{
			// set all options to their defaults 
			SetDefaults();

			// check to see if there is a options file to load 
			if (File.Exists(Helper.ResolvePath(m_OptionsFileName)) == false)
			{
				return;
			}

			// try to load the options
			Load();
		}

		private void SetDefaults()
		{
			// set the bound to empty
			Bounds = Rectangle.Empty;

			// normal window state
			WindowState = FormWindowState.Normal;

			From = String.Empty;

			To = String.Empty;

			Credits = 0;
		
			TotalHoldSpace = 1;

			UsedHoldSpace = 0;

			SortColumn = null;

			SortDirection = SortOrder.Descending; 
		}

		private void Load()
		{
			try
			{
				XmlDocument doc = new XmlDocument();

				// load the options from the resolved path
				doc.Load(Helper.ResolvePath(m_OptionsFileName));

				XmlNode node = doc.DocumentElement;

				// if the node is not null 
				if (node != null)
				{
					From = Helper.GetAttributeValue(node, "From", From);
					To = Helper.GetAttributeValue(node, "To", To);
					Credits = Helper.GetAttributeValue(node, "Credits", Credits);
					TotalHoldSpace = Helper.GetAttributeValue(node, "TotalHoldSpace", TotalHoldSpace);
					UsedHoldSpace = Helper.GetAttributeValue(node, "UsedHoldSpace", UsedHoldSpace);

					SortColumn = Helper.GetAttributeValue(node, "SortColumn", SortColumn);
					SortDirection = Helper.GetAttributeValue(node, "SortDirection", SortDirection);

					// get rhe string for the bounding rectangle
					string boundsString = Helper.GetAttributeValue(node, "Bounds", Helper.SerializeRectangle(Bounds));

					try
					{
						// deserialize the bounding rectangle
						Bounds = Helper.DeserializeRectangle(boundsString);

						// if the bounds is not empty
						if (Bounds != Rectangle.Empty)
						{
							// check that the bounds is on the screen
							if (IsOnScreen(Bounds) == false)
							{
								// if the bounds is off the screen set it to empty 
								Bounds = Rectangle.Empty;
							}
						}
					}
					catch (Exception ex)
					{
						// tell the user
						MessageBox.Show(ex.Message, "Could not parse window bounds");
					}

					// get the window state
					WindowState = Helper.GetAttributeValue(node, "WindowState", WindowState);
				}
			}
			catch (Exception ex)
			{
				// somthing went wrong, tell the user
				MessageBox.Show(ex.Message, "Could not load options");
			}
		}

		public void Save()
		{
			try
			{
				XmlDocument doc = new XmlDocument();

				XmlElement node = Helper.CreateElement(doc, "Options");

				Helper.AppendAttributeAndValue(node, "From", From);
				Helper.AppendAttributeAndValue(node, "To", To);
				Helper.AppendAttributeAndValue(node, "Credits", Credits);
				Helper.AppendAttributeAndValue(node, "TotalHoldSpace", TotalHoldSpace);
				Helper.AppendAttributeAndValue(node, "UsedHoldSpace", UsedHoldSpace);

				Helper.AppendAttributeAndValue(node, "SortColumn", SortColumn);
				Helper.AppendAttributeAndValue(node, "SortDirection", SortDirection);
				
				Helper.AppendAttributeAndValue(node, "Bounds", Helper.SerializeRectangle(Bounds));
				Helper.AppendAttributeAndValue(node, "WindowState", WindowState.ToString());

				doc.AppendChild(node);

				Helper.EnsurePathExists(Helper.ResolvePath(m_OptionsFileName));
				doc.Save(Helper.ResolvePath(m_OptionsFileName));
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message, "Could not save options");
			}
		}

		/// <summary>
		/// Check that a rectangle is fully on the screen
		/// </summary>
		/// <param name="rectangle">the rectangle to check</param>
		/// <returns>true if the rectangle is fully on a screen</returns>
		private bool IsOnScreen(Rectangle rectangle)
		{
			Screen[] screens = Screen.AllScreens;
			foreach (Screen screen in screens)
			{
				if (screen.WorkingArea.Contains(rectangle))
				{
					return true;
				}
			}

			return false;
		}
	}
}
