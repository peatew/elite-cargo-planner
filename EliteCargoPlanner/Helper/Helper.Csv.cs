﻿using System.Globalization;
using System.IO;
using System.Text;

namespace EliteCargoPlanner
{
	public static partial class Helper
	{
		public static string[] ReadAllLines(string path)
		{
			return File.ReadAllLines(Helper.ResolvePath(path));
		}

		public static string ReadAllText(string path)
		{
			return File.ReadAllText(Helper.ResolvePath(path));
		}

		public static void WriteAllLines(string path, string[] lines)
		{
			Helper.EnsurePathExists(Helper.ResolvePath(path));

			File.WriteAllLines(Helper.ResolvePath(path), lines);
		}

		public static void WriteAllText(string path, string text)
		{
			Helper.EnsurePathExists(Helper.ResolvePath(path));

			File.WriteAllText(Helper.ResolvePath(path), text);
		}

		public static string[] SplitCsv(string line)
		{
			return SplitCsv(line, ',');
		}

		public static string[] SplitCsv(string line, char seperator)
		{
			return line.Split(seperator);
		}

		public static string ToCsv(params object[] cells)
		{
			return ToCsv(',', cells);
		}

		public static string ToCsv(char seperator, params object[] cells)
		{
			StringBuilder sb = new StringBuilder();

			string seperatorString = seperator + " "; 

			foreach (object obj in cells)
			{				
				if (obj is bool)
				{
					sb.Append(((bool)obj).ToString(CultureInfo.InvariantCulture) + seperatorString);
				}
				else if (obj is int)
				{
					sb.Append(((int)obj).ToString(CultureInfo.InvariantCulture) + seperatorString);
				}
				else if (obj is uint)
				{
					sb.Append(((uint)obj).ToString(CultureInfo.InvariantCulture) + seperatorString);
				}
				else if (obj is long)
				{
					sb.Append(((long)obj).ToString(CultureInfo.InvariantCulture) + seperatorString);
				}
				else if (obj is ulong)
				{
					sb.Append(((ulong)obj).ToString(CultureInfo.InvariantCulture) + seperatorString);
				}
				else if (obj is short)
				{
					sb.Append(((short)obj).ToString(CultureInfo.InvariantCulture) + seperatorString);
				}
				else if (obj is ushort)
				{
					sb.Append(((ushort)obj).ToString(CultureInfo.InvariantCulture) + seperatorString);
				}
				else if (obj is float)
				{
					sb.Append(((float)obj).ToString(CultureInfo.InvariantCulture) + seperatorString);
				}
				else if (obj is double)
				{
					sb.Append(((double)obj).ToString(CultureInfo.InvariantCulture) + seperatorString);
				}
				else if (obj is decimal)
				{
					sb.Append(((decimal)obj).ToString(CultureInfo.InvariantCulture) + seperatorString);
				}
				else 
				{
					sb.Append(obj.ToString() + seperatorString);
				}
			}

			string final = sb.ToString();

			// strip of the final seperator string 
			final = final.Substring(0, final.Length - seperatorString.Length); 

			return final; 
		}
	}
}
