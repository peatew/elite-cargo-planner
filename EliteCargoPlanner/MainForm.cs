﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;

namespace EliteCargoPlanner
{
	public partial class MainForm : Form
	{
		Market m_FromMarket, m_ToMarket;

		DataGridViewCellStyle[] m_CellStyles;
		DataGridViewCellStyle m_NoData; 

		float m_ProfitMargin = 750f;
		private DateTime m_StartTime; 

		public MainForm()
		{
			InitializeComponent();
		}

		private void MainForm_Load(object sender, EventArgs e)
		{
			if (StaticObject.Options.Bounds != Rectangle.Empty)
			{
				this.DesktopBounds = StaticObject.Options.Bounds;
			}

			WindowState = StaticObject.Options.WindowState;

			m_CellStyles = new DataGridViewCellStyle[] 
			{
				new DataGridViewCellStyle() { BackColor = Color.Red }, 
				new DataGridViewCellStyle() { BackColor = Color.Orange }, 
				new DataGridViewCellStyle() { BackColor = Color.Gold }, 
				new DataGridViewCellStyle() { BackColor = Color.White }, 
				new DataGridViewCellStyle() { BackColor = Color.LightGreen }, 
				new DataGridViewCellStyle() { BackColor = Color.GreenYellow }, 
				new DataGridViewCellStyle() { BackColor = Color.Green }, 
			};

			m_NoData = new DataGridViewCellStyle() { ForeColor = Color.LightGray };

			this.Commodity.ValueType = typeof(string);
			this.Average.ValueType = typeof(float);
			this.FromSell.ValueType = typeof(float);
			this.FromBuy.ValueType = typeof(float);
			this.ToSell.ValueType = typeof(float);
			this.ToBuy.ValueType = typeof(float);
			this.Profit.ValueType = typeof(float);
			this.Profit.DefaultCellStyle.NullValue = "--";

			this.Risk.ValueType = typeof(float);
			this.Risk.DefaultCellStyle.Format = "P";
			this.Risk.DefaultCellStyle.NullValue = "--";

			this.Max.ValueType = typeof(int);
			this.Max.DefaultCellStyle.NullValue = "--";

			this.Haul.ValueType = typeof(float);
			this.Haul.DefaultCellStyle.NullValue = "--";

			// Attach DataGridView events to the corresponding event handlers.
			this.m_Grid.CellValidating += new DataGridViewCellValidatingEventHandler(Grid_CellValidating);
			this.m_Grid.CellEndEdit += new DataGridViewCellEventHandler(Grid_CellEndEdit);

			m_Grid.CellEndEdit +=new DataGridViewCellEventHandler(Grid_CellEndEdit);
			m_Grid.DefaultValuesNeeded += new DataGridViewRowEventHandler(Grid_DefaultValuesNeeded);
			m_Grid.Sorted += new EventHandler(Grid_Sorted);

			fromSelector.Selected = StaticObject.Options.From;
			toSelector.Selected = StaticObject.Options.To;

			m_Notes.Text = StaticObject.Notes;
	
			m_Credits.Value = StaticObject.Options.Credits;
			m_TotalHoldSpace.Value = StaticObject.Options.TotalHoldSpace;
			m_UsedHoldSpace.Value = StaticObject.Options.UsedHoldSpace;

			if (StaticObject.Options.SortColumn != null) 
			{
				m_Grid.Sort(
					m_Grid.Columns[StaticObject.Options.SortColumn], 
					StaticObject.Options.SortDirection == SortOrder.Ascending ? ListSortDirection.Ascending : ListSortDirection.Descending
					);
			}

			this.m_TotalHoldSpace.ValueChanged += new System.EventHandler(this.MaxCargo_ValueChanged);
			this.m_UsedHoldSpace.ValueChanged += new System.EventHandler(this.UsedCargo_ValueChanged);
			this.m_Credits.ValueChanged += new System.EventHandler(this.Credits_ValueChanged);

			this.toSelector.SelectionChanged += new System.EventHandler(this.ToSelector_SelectionChanged);
			this.fromSelector.SelectionChanged += new System.EventHandler(this.FromSelector_SelectionChanged);

			
			//this.TopMost = true; 

			UpdateGrid(); 
		}

		void Grid_Sorted(object sender, EventArgs e)
		{
			if (m_Grid.SortedColumn == null) 
			{
				return; 
			}

			StaticObject.Options.SortColumn = m_Grid.SortedColumn.Name;
			StaticObject.Options.SortDirection = m_Grid.SortOrder; 
		}

		#region Window Resize / Move Events

		private void MainForm_ResizeBegin(object sender, EventArgs e)
		{

		}

		private void MainForm_ResizeEnd(object sender, EventArgs e)
		{
			if (WindowState == FormWindowState.Normal)
			{
				StaticObject.Options.Bounds = this.DesktopBounds;
			}
		}

		private void MainForm_Resize(object sender, EventArgs e)
		{
			if (WindowState != FormWindowState.Minimized)
			{
				StaticObject.Options.WindowState = WindowState;
			}
		}

		#endregion

		void Grid_DefaultValuesNeeded(object sender, DataGridViewRowEventArgs e)
		{
			e.Row.Cells[0].Value = "Unnamed";
			e.Row.Cells[1].Value = 0f;
			e.Row.Cells[2].Value = 0f;
			e.Row.Cells[3].Value = 0f;
			e.Row.Cells[4].Value = 0f;
			e.Row.Cells[5].Value = 0f;
			e.Row.Cells[6].Value = null;
			e.Row.Cells[7].Value = null;
			e.Row.Cells[8].Value = null;
			e.Row.Cells[9].Value = null;
		}

		void Grid_CellEndEdit(object sender, DataGridViewCellEventArgs e)
		{
			// Clear the row error in case the user presses ESC.   
			m_Grid.Rows[e.RowIndex].ErrorText = String.Empty;

			if (m_Grid.Rows[e.RowIndex].Cells[0].Value.Equals("Unnamed") == true)
			{
				return; 
			}

			string name = m_Grid.Rows[e.RowIndex].Cells[0].Value.ToString();
			float average = (float)m_Grid.Rows[e.RowIndex].Cells[1].Value;
			
			float fromSell = (float)m_Grid.Rows[e.RowIndex].Cells[2].Value;
			float fromBuy = (float)m_Grid.Rows[e.RowIndex].Cells[3].Value;
			
			float toSell = (float)m_Grid.Rows[e.RowIndex].Cells[4].Value;
			float toBuy = (float)m_Grid.Rows[e.RowIndex].Cells[5].Value;

			if (StaticObject.Commodities.ContainsKey(name) == false)
			{
				StaticObject.Commodities.Add(name, new Commodity(name)); 
			}

			StaticObject.Commodities[name].Average = average;

			if (m_FromMarket == null)
			{
				return; 
			}

			if (m_ToMarket == null)
			{
				return; 
			}

			if (m_FromMarket.Commodities.ContainsKey(name) == false)
			{
				m_FromMarket.Commodities.Add(name, new MarketCommodity(name));
			}

			if (m_ToMarket.Commodities.ContainsKey(name) == false)
			{
				m_ToMarket.Commodities.Add(name, new MarketCommodity(name));
			}

			m_FromMarket.Commodities[name].Sell = fromSell;
			m_FromMarket.Commodities[name].Buy = fromBuy;

			m_ToMarket.Commodities[name].Sell = toSell;
			m_ToMarket.Commodities[name].Buy = toBuy;

			UpdateRow(e.RowIndex); 

			//m_Grid.Update(); 
		}

		private void UpdateRow(int index)
		{
			if (m_Grid.Rows[index].Cells[0].Value == null)
			{
				return; 
			}

			string name = m_Grid.Rows[index].Cells[0].Value.ToString();
			float average = (float)m_Grid.Rows[index].Cells[1].Value;

			float fromSell = (float)m_Grid.Rows[index].Cells[2].Value;
			float fromBuy = (float)m_Grid.Rows[index].Cells[3].Value;

			float toSell = (float)m_Grid.Rows[index].Cells[4].Value;
			float toBuy = (float)m_Grid.Rows[index].Cells[5].Value;

			m_Grid.Rows[index].Cells[2].Style = (average == 0 || fromSell == 0) ? m_NoData : GetCellStyle(average - fromSell);
			m_Grid.Rows[index].Cells[3].Style = (average == 0 || fromBuy == 0) ? m_NoData : GetCellStyle(average - fromBuy);

			m_Grid.Rows[index].Cells[4].Style = (average == 0 || toSell == 0) ? m_NoData : GetCellStyle(toSell - average);
			m_Grid.Rows[index].Cells[5].Style = (average == 0 || toBuy == 0) ? m_NoData : GetCellStyle(toBuy - average);

			float profit = 0; 

			if (toSell != 0 && fromBuy != 0)
			{
				profit = toSell - fromBuy;

				m_Grid.Rows[index].Cells[6].Value = profit;
				m_Grid.Rows[index].Cells[6].Style = GetCellStyle(profit);
			}
			else
			{
				m_Grid.Rows[index].Cells[6].Value = null;
				m_Grid.Rows[index].Cells[6].Style = m_NoData;
			}

			if (profit != 0 && fromBuy != 0)
			{
				float risk = profit / fromBuy;

				m_Grid.Rows[index].Cells[7].Value = risk;
				m_Grid.Rows[index].Cells[7].Style = GetCellStyle(risk * m_ProfitMargin);
			}
			else
			{
				m_Grid.Rows[index].Cells[7].Value = null;
				m_Grid.Rows[index].Cells[7].Style = m_NoData; 
			}

			if (fromBuy != 0 && toSell != 0)
			{
				int amount = (int)((float)m_Credits.Value / fromBuy);

				var holdSpace = (m_TotalHoldSpace.Value - m_UsedHoldSpace.Value);

				amount = Math.Min((int)holdSpace, amount);

				float proportionOfHold = ((float)amount / (float)holdSpace) * 2f - 1f;

				m_Grid.Rows[index].Cells[8].Value = amount;
				m_Grid.Rows[index].Cells[8].Style = GetCellStyle(proportionOfHold * m_ProfitMargin);

				float haul = (float)amount * (toSell - fromBuy);

				m_Grid.Rows[index].Cells[9].Value = haul;
				m_Grid.Rows[index].Cells[9].Style = GetCellStyle((haul / (float)holdSpace));				

			}
			else
			{
				m_Grid.Rows[index].Cells[8].Value = null;
				m_Grid.Rows[index].Cells[8].Style = m_NoData;

				m_Grid.Rows[index].Cells[9].Value = null;
				m_Grid.Rows[index].Cells[9].Style = m_NoData; 
			}

		}

		private DataGridViewCellStyle GetCellStyle(float value)
		{
			value = Math.Max(-m_ProfitMargin, Math.Min(m_ProfitMargin, value));

			value /= m_ProfitMargin;

			value *= 0.5f;

			value += 0.5f;

			value *= m_CellStyles.Length - 1; 

			int index = Math.Max(0, Math.Min(m_CellStyles.Length - 1, (int)value));

			return m_CellStyles[index];
		}

		void Grid_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
		{
			// Validate the CompanyName entry by disallowing empty strings.
			if (m_Grid.Columns[e.ColumnIndex].Name == "Commodity")
			{
				if (String.IsNullOrEmpty(e.FormattedValue.ToString()))
				{
					m_Grid.Rows[e.RowIndex].ErrorText = "Commodity name must not be empty";
					e.Cancel = true;
				}
			}
			else if (m_Grid.Columns[e.ColumnIndex].ReadOnly == false)
			{
				float value; 

				if (float.TryParse(e.FormattedValue.ToString(), out value) == false)
				{
					m_Grid.Rows[e.RowIndex].ErrorText = "Invalid value";
					e.Cancel = true;					
				}
			}
		}

		private void button1_Click(object sender, EventArgs e)
		{
			Market market1, market2;

			if (StaticObject.Markets.TryGetValue(fromSelector.Selected, out market1) == false)
			{
				if (Helper.IsNullOrEmpty(fromSelector.Selected) == true)
				{
					return; 
				}

				market1 = new Market(fromSelector.Selected);

				StaticObject.Markets.Add(market1.Name, market1);
			}

			if (StaticObject.Markets.TryGetValue(toSelector.Selected, out market2) == false)
			{
				if (Helper.IsNullOrEmpty(toSelector.Selected) == true)
				{
					return;
				}

				market2 = new Market(toSelector.Selected);

				StaticObject.Markets.Add(market2.Name, market2);				
			}

			this.toSelector.SelectionChanged -= new System.EventHandler(this.ToSelector_SelectionChanged);
			this.fromSelector.SelectionChanged -= new System.EventHandler(this.FromSelector_SelectionChanged);

			fromSelector.Selected = market2.Name;
			toSelector.Selected = market1.Name;

			this.toSelector.SelectionChanged += new System.EventHandler(this.ToSelector_SelectionChanged);
			this.fromSelector.SelectionChanged += new System.EventHandler(this.FromSelector_SelectionChanged);

			UpdateGrid(); 

			StaticObject.UpdateMarkets();
		}

		private void FromSelector_SelectionChanged(object sender, EventArgs e)
		{
			StaticObject.Options.From = fromSelector.Selected; 

			UpdateGrid(); 
		}

		private void ToSelector_SelectionChanged(object sender, EventArgs e)
		{
			StaticObject.Options.To = toSelector.Selected; 

			UpdateGrid(); 
		}

		private void UpdateGrid()
		{
			if (m_Grid.CommitEdit(DataGridViewDataErrorContexts.CurrentCellChange) == false)
			{
				return; 
			}

			m_Grid.SuspendLayout(); 

			DataGridViewColumn sortColumn = m_Grid.SortedColumn; 
			SortOrder sortOrder = m_Grid.SortOrder; 

			m_Grid.Rows.Clear();			

			m_FromMarket = null;
			m_ToMarket = null;

			m_FromNotes.Text = String.Empty;
			m_ToNotes.Text = String.Empty;

			m_FromNotes.ReadOnly = true; 
			m_ToNotes.ReadOnly = true;			

			try
			{
				Market fromMarket = null;
				Market toMarket = null;

				if (StaticObject.Markets.TryGetValue(fromSelector.Selected, out fromMarket) == false)
				{
					if (Helper.IsNullOrEmpty(fromSelector.Selected) == true)
					{
						return;
					}

					fromMarket = new Market(fromSelector.Selected);

					StaticObject.Markets.Add(fromMarket.Name, fromMarket);
				}

				if (StaticObject.Markets.TryGetValue(toSelector.Selected, out toMarket) == false)
				{
					if (Helper.IsNullOrEmpty(toSelector.Selected) == true)
					{
						return;
					}

					toMarket = new Market(toSelector.Selected);

					StaticObject.Markets.Add(toMarket.Name, toMarket);
				}

				m_FromNotes.Text = fromMarket.Notes;
				m_ToNotes.Text = toMarket.Notes;

				m_FromNotes.ReadOnly = false;
				m_ToNotes.ReadOnly = false;

				m_FromMarket = fromMarket;
				m_ToMarket = toMarket;

				List<string> allCommodities = new List<string>();

				foreach (Commodity comm in StaticObject.Commodities.Values)
				{
					if (allCommodities.Contains(comm.Name) == false)
					{
						allCommodities.Add(comm.Name);
					}
				}

				foreach (MarketCommodity comm in m_FromMarket.Commodities.Values)
				{
					if (allCommodities.Contains(comm.Name) == false)
					{
						allCommodities.Add(comm.Name);
					}
				}

				foreach (MarketCommodity comm in m_ToMarket.Commodities.Values)
				{
					if (allCommodities.Contains(comm.Name) == false)
					{
						allCommodities.Add(comm.Name);
					}
				}

				foreach (string comm in allCommodities)
				{
					if (StaticObject.Commodities.ContainsKey(comm) == false)
					{
						StaticObject.Commodities.Add(comm, new Commodity(comm));
					}

					if (m_FromMarket.Commodities.ContainsKey(comm) == false)
					{
						m_FromMarket.Commodities.Add(comm, new MarketCommodity(comm));
					}

					if (m_ToMarket.Commodities.ContainsKey(comm) == false)
					{
						m_ToMarket.Commodities.Add(comm, new MarketCommodity(comm));
					}
				}

				allCommodities.Sort();

				foreach (string comm in allCommodities)
				{
					Commodity galacitc = StaticObject.Commodities[comm];

					MarketCommodity from = m_FromMarket.Commodities[comm];
					MarketCommodity to = m_ToMarket.Commodities[comm];

					m_Grid.Rows.Add(comm, galacitc.Average, from.Sell, from.Buy, to.Sell, to.Buy, null, null, null, null);
				}

				for (int i = 0; i < m_Grid.Rows.Count; i++)
				{
					UpdateRow(i);
				}
			}
			catch { }
			finally
			{			
				if (sortColumn != null)
				{
					m_Grid.Sort(sortColumn, sortOrder == SortOrder.Ascending ? System.ComponentModel.ListSortDirection.Ascending : System.ComponentModel.ListSortDirection.Descending);
				}

				//m_Grid.Update();

				m_Grid.ResumeLayout();
			}
		}

		private void FromNotes_TextChanged(object sender, EventArgs e)
		{
			if (m_FromMarket == null)
			{
				return; 
			}

			m_FromMarket.Notes = m_FromNotes.Text;
		}

		private void ToNotes_TextChanged(object sender, EventArgs e)
		{
			if (m_ToMarket == null)
			{
				return;
			}

			m_ToMarket.Notes = m_ToNotes.Text;
		}

		private void UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
		{
			string name = e.Row.Cells[0].Value.ToString();

			if (name == "Unnamed")
			{
				return; 
			}

			if (MessageBox.Show("Purge commodity '" + name + "' from the database", "Purge Commodity", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.Cancel)
			{
				e.Cancel = true;

				return; 
			}
		}

		private void UserDeletedRow(object sender, DataGridViewRowEventArgs e)
		{
			string name = e.Row.Cells[0].Value.ToString();

			if (name == "Unnamed")
			{
				return;
			}

			StaticObject.Commodities.Remove(name);

			foreach (Market market in StaticObject.Markets.Values)
			{
				market.Commodities.Remove(name);
			}

			UpdateGrid();
		}

		private void Credits_ValueChanged(object sender, EventArgs e)
		{
			StaticObject.Options.Credits = (int)m_Credits.Value; 

			UpdateGrid();
		}

		private void MaxCargo_ValueChanged(object sender, EventArgs e)
		{
			StaticObject.Options.TotalHoldSpace = (int)m_TotalHoldSpace.Value;

			UpdateGrid();
		}

		private void UsedCargo_ValueChanged(object sender, EventArgs e)
		{
			StaticObject.Options.UsedHoldSpace = (int)m_UsedHoldSpace.Value;

			UpdateGrid();
		}

		private void timer1_Tick(object sender, EventArgs e)
		{
			m_TimerBox.Text = DateTime.Now.Subtract(m_StartTime).ToString(); 
		}

		private void m_Start_Click(object sender, EventArgs e)
		{
			timer1.Enabled = true;
			
			m_StartTime = DateTime.Now; 
		}

		private void m_Stop_Click(object sender, EventArgs e)
		{
			timer1.Enabled = false; 
		}

		private void m_Notes_TextChanged(object sender, EventArgs e)
		{
			StaticObject.Notes = m_Notes.Text; 
		}
	}
}
