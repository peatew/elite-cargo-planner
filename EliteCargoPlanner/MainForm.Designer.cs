﻿namespace EliteCargoPlanner
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.splitContainer2 = new System.Windows.Forms.SplitContainer();
			this.m_FromNotes = new System.Windows.Forms.TextBox();
			this.button1 = new System.Windows.Forms.Button();
			this.fromSelector = new EliteCargoPlanner.MarketSelector();
			this.toSelector = new EliteCargoPlanner.MarketSelector();
			this.m_ToNotes = new System.Windows.Forms.TextBox();
			this.m_Grid = new GridControl();
			this.Commodity = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Average = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.FromSell = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.FromBuy = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ToSell = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ToBuy = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Profit = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Risk = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Max = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Haul = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.splitContainer3 = new System.Windows.Forms.SplitContainer();
			this.m_Notes = new System.Windows.Forms.TextBox();
			this.m_TimerBox = new System.Windows.Forms.TextBox();
			this.m_Stop = new System.Windows.Forms.Button();
			this.m_Start = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.m_Credits = new System.Windows.Forms.NumericUpDown();
			this.m_UsedHoldSpace = new System.Windows.Forms.NumericUpDown();
			this.m_TotalHoldSpace = new System.Windows.Forms.NumericUpDown();
			this.label1 = new System.Windows.Forms.Label();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.splitContainer2.Panel1.SuspendLayout();
			this.splitContainer2.Panel2.SuspendLayout();
			this.splitContainer2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.m_Grid)).BeginInit();
			this.splitContainer3.Panel1.SuspendLayout();
			this.splitContainer3.Panel2.SuspendLayout();
			this.splitContainer3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.m_Credits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.m_UsedHoldSpace)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.m_TotalHoldSpace)).BeginInit();
			this.SuspendLayout();
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
			this.splitContainer1.Location = new System.Drawing.Point(0, 0);
			this.splitContainer1.Name = "splitContainer1";
			this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.m_Grid);
			this.splitContainer1.Panel2.Font = new System.Drawing.Font("Courier New", 7F);
			this.splitContainer1.Size = new System.Drawing.Size(1126, 475);
			this.splitContainer1.SplitterDistance = 81;
			this.splitContainer1.TabIndex = 0;
			// 
			// splitContainer2
			// 
			this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer2.IsSplitterFixed = true;
			this.splitContainer2.Location = new System.Drawing.Point(0, 0);
			this.splitContainer2.Name = "splitContainer2";
			// 
			// splitContainer2.Panel1
			// 
			this.splitContainer2.Panel1.Controls.Add(this.m_FromNotes);
			this.splitContainer2.Panel1.Controls.Add(this.button1);
			this.splitContainer2.Panel1.Controls.Add(this.fromSelector);
			// 
			// splitContainer2.Panel2
			// 
			this.splitContainer2.Panel2.Controls.Add(this.toSelector);
			this.splitContainer2.Panel2.Controls.Add(this.m_ToNotes);
			this.splitContainer2.Size = new System.Drawing.Size(1126, 81);
			this.splitContainer2.SplitterDistance = 566;
			this.splitContainer2.TabIndex = 0;
			// 
			// m_FromNotes
			// 
			this.m_FromNotes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.m_FromNotes.Font = new System.Drawing.Font("Courier New", 7F);
			this.m_FromNotes.Location = new System.Drawing.Point(3, 32);
			this.m_FromNotes.Multiline = true;
			this.m_FromNotes.Name = "m_FromNotes";
			this.m_FromNotes.ReadOnly = true;
			this.m_FromNotes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.m_FromNotes.Size = new System.Drawing.Size(519, 46);
			this.m_FromNotes.TabIndex = 1;
			this.m_FromNotes.TextChanged += new System.EventHandler(this.FromNotes_TextChanged);
			// 
			// button1
			// 
			this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.button1.Location = new System.Drawing.Point(524, 5);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(39, 73);
			this.button1.TabIndex = 1;
			this.button1.Text = "<->";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// fromSelector
			// 
			this.fromSelector.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.fromSelector.Location = new System.Drawing.Point(0, 3);
			this.fromSelector.Margin = new System.Windows.Forms.Padding(0);
			this.fromSelector.MaximumSize = new System.Drawing.Size(100000, 26);
			this.fromSelector.MinimumSize = new System.Drawing.Size(100, 26);
			this.fromSelector.Mode = EliteCargoPlanner.MarketSelector.PlaceSelectorMode.From;
			this.fromSelector.Name = "fromSelector";
			this.fromSelector.Selected = "";
			this.fromSelector.Size = new System.Drawing.Size(522, 26);
			this.fromSelector.TabIndex = 0;
			// 
			// toSelector
			// 
			this.toSelector.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.toSelector.Location = new System.Drawing.Point(0, 3);
			this.toSelector.Margin = new System.Windows.Forms.Padding(0);
			this.toSelector.MaximumSize = new System.Drawing.Size(100000, 26);
			this.toSelector.MinimumSize = new System.Drawing.Size(100, 26);
			this.toSelector.Mode = EliteCargoPlanner.MarketSelector.PlaceSelectorMode.To;
			this.toSelector.Name = "toSelector";
			this.toSelector.Selected = "";
			this.toSelector.Size = new System.Drawing.Size(556, 26);
			this.toSelector.TabIndex = 2;
			// 
			// m_ToNotes
			// 
			this.m_ToNotes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.m_ToNotes.Font = new System.Drawing.Font("Courier New", 7F);
			this.m_ToNotes.Location = new System.Drawing.Point(3, 31);
			this.m_ToNotes.Multiline = true;
			this.m_ToNotes.Name = "m_ToNotes";
			this.m_ToNotes.ReadOnly = true;
			this.m_ToNotes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.m_ToNotes.Size = new System.Drawing.Size(550, 47);
			this.m_ToNotes.TabIndex = 2;
			this.m_ToNotes.TextChanged += new System.EventHandler(this.ToNotes_TextChanged);
			// 
			// m_Grid
			// 
			dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240)))));
			this.m_Grid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
			this.m_Grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.m_Grid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Commodity,
            this.Average,
            this.FromSell,
            this.FromBuy,
            this.ToSell,
            this.ToBuy,
            this.Profit,
            this.Risk,
            this.Max,
            this.Haul});
			this.m_Grid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.m_Grid.Location = new System.Drawing.Point(0, 0);
			this.m_Grid.Name = "dataGridView1";
			this.m_Grid.RowTemplate.Height = 17;
			this.m_Grid.Size = new System.Drawing.Size(1126, 390);
			this.m_Grid.TabIndex = 0;
			this.m_Grid.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.UserDeletedRow);
			this.m_Grid.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.UserDeletingRow);
			// 
			// Commodity
			// 
			this.Commodity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
			this.Commodity.HeaderText = "Commodity Name";
			this.Commodity.MinimumWidth = 140;
			this.Commodity.Name = "Commodity";
			// 
			// Average
			// 
			this.Average.HeaderText = "Galacitc Av.";
			this.Average.Name = "Average";
			this.Average.Width = 120;
			// 
			// FromSell
			// 
			this.FromSell.HeaderText = "Sell (Cr)";
			this.FromSell.Name = "FromSell";
			// 
			// FromBuy
			// 
			this.FromBuy.HeaderText = "Buy (Cr)";
			this.FromBuy.Name = "FromBuy";
			// 
			// ToSell
			// 
			this.ToSell.HeaderText = "Sell (Cr)";
			this.ToSell.Name = "ToSell";
			// 
			// ToBuy
			// 
			this.ToBuy.HeaderText = "Buy (Cr)";
			this.ToBuy.Name = "ToBuy";
			// 
			// Profit
			// 
			this.Profit.HeaderText = "Profit (Cr)";
			this.Profit.Name = "Profit";
			this.Profit.ReadOnly = true;
			this.Profit.Width = 140;
			// 
			// Risk
			// 
			this.Risk.HeaderText = "Profit (%)";
			this.Risk.Name = "Risk";
			this.Risk.ReadOnly = true;
			this.Risk.Width = 120;
			// 
			// Max
			// 
			this.Max.HeaderText = "Max";
			this.Max.Name = "Max";
			this.Max.ReadOnly = true;
			this.Max.Width = 50;
			// 
			// Haul
			// 
			this.Haul.HeaderText = "Haul (Cr)";
			this.Haul.Name = "Haul";
			this.Haul.ReadOnly = true;
			// 
			// splitContainer3
			// 
			this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer3.Location = new System.Drawing.Point(0, 0);
			this.splitContainer3.Name = "splitContainer3";
			// 
			// splitContainer3.Panel1
			// 
			this.splitContainer3.Panel1.Controls.Add(this.splitContainer1);
			// 
			// splitContainer3.Panel2
			// 
			this.splitContainer3.Panel2.Controls.Add(this.m_Notes);
			this.splitContainer3.Panel2.Controls.Add(this.m_TimerBox);
			this.splitContainer3.Panel2.Controls.Add(this.m_Stop);
			this.splitContainer3.Panel2.Controls.Add(this.m_Start);
			this.splitContainer3.Panel2.Controls.Add(this.label3);
			this.splitContainer3.Panel2.Controls.Add(this.label2);
			this.splitContainer3.Panel2.Controls.Add(this.m_Credits);
			this.splitContainer3.Panel2.Controls.Add(this.m_UsedHoldSpace);
			this.splitContainer3.Panel2.Controls.Add(this.m_TotalHoldSpace);
			this.splitContainer3.Panel2.Controls.Add(this.label1);
			this.splitContainer3.Size = new System.Drawing.Size(1322, 475);
			this.splitContainer3.SplitterDistance = 1126;
			this.splitContainer3.TabIndex = 1;
			// 
			// m_Notes
			// 
			this.m_Notes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.m_Notes.Font = new System.Drawing.Font("Courier New", 7F);
			this.m_Notes.Location = new System.Drawing.Point(2, 83);
			this.m_Notes.Multiline = true;
			this.m_Notes.Name = "m_Notes";
			this.m_Notes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.m_Notes.Size = new System.Drawing.Size(187, 359);
			this.m_Notes.TabIndex = 3;
			this.m_Notes.TextChanged += new System.EventHandler(this.m_Notes_TextChanged);
			// 
			// m_TimerBox
			// 
			this.m_TimerBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.m_TimerBox.Font = new System.Drawing.Font("Courier New", 8F);
			this.m_TimerBox.Location = new System.Drawing.Point(5, 448);
			this.m_TimerBox.Name = "m_TimerBox";
			this.m_TimerBox.Size = new System.Drawing.Size(134, 20);
			this.m_TimerBox.TabIndex = 11;
			// 
			// m_Stop
			// 
			this.m_Stop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.m_Stop.Location = new System.Drawing.Point(168, 446);
			this.m_Stop.Name = "m_Stop";
			this.m_Stop.Size = new System.Drawing.Size(21, 23);
			this.m_Stop.TabIndex = 10;
			this.m_Stop.Text = "||";
			this.m_Stop.UseVisualStyleBackColor = true;
			this.m_Stop.Click += new System.EventHandler(this.m_Stop_Click);
			// 
			// m_Start
			// 
			this.m_Start.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.m_Start.Location = new System.Drawing.Point(145, 446);
			this.m_Start.Name = "m_Start";
			this.m_Start.Size = new System.Drawing.Size(21, 23);
			this.m_Start.TabIndex = 9;
			this.m_Start.Text = ">";
			this.m_Start.UseVisualStyleBackColor = true;
			this.m_Start.Click += new System.EventHandler(this.m_Start_Click);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(3, 59);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(32, 13);
			this.label3.TabIndex = 8;
			this.label3.Text = "Used";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(3, 33);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(27, 13);
			this.label2.TabIndex = 7;
			this.label2.Text = "Max";
			// 
			// m_Credits
			// 
			this.m_Credits.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.m_Credits.Location = new System.Drawing.Point(40, 5);
			this.m_Credits.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
			this.m_Credits.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.m_Credits.Name = "m_Credits";
			this.m_Credits.Size = new System.Drawing.Size(149, 20);
			this.m_Credits.TabIndex = 3;
			this.m_Credits.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// m_UsedHoldSpace
			// 
			this.m_UsedHoldSpace.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.m_UsedHoldSpace.Location = new System.Drawing.Point(40, 57);
			this.m_UsedHoldSpace.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
			this.m_UsedHoldSpace.Name = "m_UsedHoldSpace";
			this.m_UsedHoldSpace.Size = new System.Drawing.Size(149, 20);
			this.m_UsedHoldSpace.TabIndex = 6;
			// 
			// m_TotalHoldSpace
			// 
			this.m_TotalHoldSpace.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.m_TotalHoldSpace.Location = new System.Drawing.Point(40, 31);
			this.m_TotalHoldSpace.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
			this.m_TotalHoldSpace.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.m_TotalHoldSpace.Name = "m_TotalHoldSpace";
			this.m_TotalHoldSpace.Size = new System.Drawing.Size(149, 20);
			this.m_TotalHoldSpace.TabIndex = 4;
			this.m_TotalHoldSpace.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(3, 7);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(17, 13);
			this.label1.TabIndex = 5;
			this.label1.Text = "Cr";
			// 
			// timer1
			// 
			this.timer1.Interval = 50;
			this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1322, 475);
			this.Controls.Add(this.splitContainer3);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MinimumSize = new System.Drawing.Size(317, 279);
			this.Name = "MainForm";
			this.Text = "Elite Cargo Planner";
			this.Load += new System.EventHandler(this.MainForm_Load);
			this.ResizeBegin += new System.EventHandler(this.MainForm_ResizeBegin);
			this.ResizeEnd += new System.EventHandler(this.MainForm_ResizeEnd);
			this.Resize += new System.EventHandler(this.MainForm_Resize);
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			this.splitContainer1.ResumeLayout(false);
			this.splitContainer2.Panel1.ResumeLayout(false);
			this.splitContainer2.Panel1.PerformLayout();
			this.splitContainer2.Panel2.ResumeLayout(false);
			this.splitContainer2.Panel2.PerformLayout();
			this.splitContainer2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.m_Grid)).EndInit();
			this.splitContainer3.Panel1.ResumeLayout(false);
			this.splitContainer3.Panel2.ResumeLayout(false);
			this.splitContainer3.Panel2.PerformLayout();
			this.splitContainer3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.m_Credits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.m_UsedHoldSpace)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.m_TotalHoldSpace)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.SplitContainer splitContainer1;
		private MarketSelector fromSelector;
		private System.Windows.Forms.SplitContainer splitContainer2;
		private System.Windows.Forms.Button button1;
		private MarketSelector toSelector;
		private GridControl m_Grid;
		private System.Windows.Forms.SplitContainer splitContainer3;
		private System.Windows.Forms.TextBox m_FromNotes;
		private System.Windows.Forms.TextBox m_ToNotes;
		private System.Windows.Forms.NumericUpDown m_Credits;
		private System.Windows.Forms.NumericUpDown m_TotalHoldSpace;
		private System.Windows.Forms.DataGridViewTextBoxColumn Commodity;
		private System.Windows.Forms.DataGridViewTextBoxColumn Average;
		private System.Windows.Forms.DataGridViewTextBoxColumn FromSell;
		private System.Windows.Forms.DataGridViewTextBoxColumn FromBuy;
		private System.Windows.Forms.DataGridViewTextBoxColumn ToSell;
		private System.Windows.Forms.DataGridViewTextBoxColumn ToBuy;
		private System.Windows.Forms.DataGridViewTextBoxColumn Profit;
		private System.Windows.Forms.DataGridViewTextBoxColumn Risk;
		private System.Windows.Forms.DataGridViewTextBoxColumn Max;
		private System.Windows.Forms.DataGridViewTextBoxColumn Haul;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.NumericUpDown m_UsedHoldSpace;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox m_Notes;
		private System.Windows.Forms.TextBox m_TimerBox;
		private System.Windows.Forms.Button m_Stop;
		private System.Windows.Forms.Button m_Start;
		private System.Windows.Forms.Timer timer1;
	}
}

