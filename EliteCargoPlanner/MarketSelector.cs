﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace EliteCargoPlanner
{
	public partial class MarketSelector : UserControl
	{
		public enum PlaceSelectorMode { From, To }

		public PlaceSelectorMode Mode { get; set; }

		public event EventHandler SelectionChanged;

		[Browsable(false)] 
		public string Selected { get { return comboBox1.Text; } set { comboBox1.SelectedIndex = comboBox1.Items.IndexOf(value); } } 

		public MarketSelector()
		{
			InitializeComponent();
		}

		private void PlaceSelector_Load(object sender, EventArgs e)
		{
			label1.Text = Mode.ToString();			
			comboBox1.KeyDown += new KeyEventHandler(comboBox1_KeyDown);
			comboBox1.SelectedIndexChanged += new EventHandler(comboBox1_SelectedIndexChanged);
			comboBox1.Sorted = true; 

			StaticObject.MarketsChanged += new EventHandler(StaticObject_MarketsChanged);

			StaticObject_MarketsChanged(null, EventArgs.Empty); 
		}

		void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (SelectionChanged != null)
			{
				SelectionChanged(this, e); 
			}
		}

		void StaticObject_MarketsChanged(object sender, EventArgs e)
		{
			foreach (Market market in StaticObject.Markets.Values)
			{
				if (comboBox1.Items.Contains(market.Name) == false)
				{
					comboBox1.Items.Add(market.Name); 
				}
			}
		}

		void comboBox1_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Enter ||
				e.KeyCode == Keys.Return)
			{
				if (Helper.IsNullOrEmpty(comboBox1.Text) == true)
				{
					return; 
				}

				e.SuppressKeyPress = true;

				if (comboBox1.Items.Contains(comboBox1.Text) == true)
				{
					comboBox1.SelectedIndex = comboBox1.Items.IndexOf(comboBox1.Text);
					return; 
				}

				comboBox1.Items.Add(comboBox1.Text);
				comboBox1.SelectedIndex = comboBox1.Items.IndexOf(comboBox1.Text);

				StaticObject.UpdateMarkets(); 
			}
		}
	}
}
