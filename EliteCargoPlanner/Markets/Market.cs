﻿using System.Collections.Generic;

namespace EliteCargoPlanner
{
	class Market
	{
		public string Name { get; private set; }

		public Dictionary<string, MarketCommodity> Commodities { get; private set; }

		public string Notes { get; set; } 

		public Market(string name)
		{
			Name = name;

			Commodities = new Dictionary<string, MarketCommodity>(); 
		}
	}
}
