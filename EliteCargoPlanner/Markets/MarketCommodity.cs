﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EliteCargoPlanner
{
	class MarketCommodity
	{
		public string Name { get; private set; }

		public float Buy { get; set; }

		public float Sell { get; set; }

		public MarketCommodity(string name)
		{
			Name = name;
		}
	}
}
