﻿
namespace EliteCargoPlanner
{
	class Commodity
	{
		public string Name { get; private set; }

		public float Average { get; set; }

		public Commodity(string name)
		{
			Name = name; 
		}
	}
}
